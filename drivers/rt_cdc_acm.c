/*
 * RT CDC ACM driver for USB4RT
 *
 * Copyright (c) Siemens AG, 2012, 2013
 * Authors:
 *  Stanislav Parfenov <stanislav.parfenov@siemens.com>
 *  Jan Kiszka <jan.kiszka@siemens.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb/cdc.h>
#include <linux/delay.h>

#include <rt_usb.h>
#include <rtdm/rtdm_driver.h>
#include <rtdm/rtserial.h>

#define MAX_DEVICES			16

#define RT_CDC_READ_URB_BUFFER_SIZE	512
#define RT_CDC_WRITE_URB_BUFFER_SIZE	512

static unsigned int vendor[MAX_DEVICES];
module_param_array(vendor, uint, NULL, 0);
MODULE_PARM_DESC(vendor, "vendor ID");

static unsigned int product[MAX_DEVICES];
module_param_array(product, uint, NULL, 0);
MODULE_PARM_DESC(vendor, "product ID");

static unsigned int start_index;
module_param(start_index, uint, 0400);
MODULE_PARM_DESC(start_index, "First device instance number to be used");

MODULE_LICENSE("GPL");

struct rt_cdc_context {
	struct usb_device *usb_dev;
	int dev_id;

	struct rtser_config config;

	rtdm_lock_t lock;

	struct rt_urb *read_urb;
	struct rt_urb_section read_section;

	struct rt_urb *write_urb;

	volatile unsigned long in_lock;
	size_t in_npend;
	size_t in_nwait;
	rtdm_event_t in_event;

	bool out_busy;
	size_t out_npend;
	rtdm_event_t out_event;
	rtdm_mutex_t out_lock;

	int status;
	int saved_errors;
	bool urb_submission_error;
};

static struct rtdm_device *device[MAX_DEVICES];

static const struct rtser_config default_config = {
	0xFFFF, RTSER_DEF_BAUD, RTSER_DEF_PARITY, RTSER_DEF_BITS,
	RTSER_DEF_STOPB, RTSER_DEF_HAND, RTSER_DEF_FIFO_DEPTH, 0,
	RTSER_DEF_TIMEOUT, RTSER_DEF_TIMEOUT, RTSER_DEF_TIMEOUT,
	RTSER_DEF_TIMESTAMP_HISTORY, RTSER_DEF_EVENT_MASK, RTSER_DEF_RS485
};

static int rt_cdc_close(struct rtdm_dev_context *context,
			rtdm_user_info_t *user_info);

static void rt_cdc_read_urb_completed(struct rt_urb *urb)
{
	struct rt_cdc_context *ctx = urb->p_context;

	rtdm_lock_get(&ctx->lock);

	if (ctx->read_urb) {
		if (urb->status == 0)
			ctx->in_npend += urb->actual_length;
		else
			ctx->status |= RTSER_LSR_FRAMING_ERR;

		if (ctx->in_nwait > 0) {
			if (ctx->in_nwait <= ctx->in_npend || ctx->status) {
				ctx->in_nwait = 0;
				rtdm_event_signal(&ctx->in_event);
			}
		}
	}

	rtdm_lock_put(&ctx->lock);
}

static void rt_cdc_write_urb_completed(struct rt_urb *urb)
{
	struct rt_cdc_context *ctx = urb->p_context;

	rtdm_lock_get(&ctx->lock);

	ctx->out_busy = false;

	if (ctx->out_npend > 0) {
		ctx->out_npend = 0;
		rtdm_event_signal(&ctx->out_event);
	}

	rtdm_lock_put(&ctx->lock);
}

static int rt_cdc_open(struct rtdm_dev_context *context,
		       rtdm_user_info_t *user_info, int oflags)
{
	struct rt_cdc_context *ctx;
	struct rt_urb *urb;
	int endpoint;
	int err = 0;

	ctx = (struct rt_cdc_context *)context->dev_private;

	ctx->dev_id = context->device->device_id;

	ctx->usb_dev = context->device->device_data;

	/* IPC initialisation - cannot fail with used parameters */
	rtdm_lock_init(&ctx->lock);
	rtdm_event_init(&ctx->in_event, 0);
	rtdm_event_init(&ctx->out_event, 0);
	rtdm_mutex_init(&ctx->out_lock);

	ctx->in_npend = 0;
	ctx->in_nwait = 0;
	ctx->in_lock = 0;

	ctx->out_busy = false;
	ctx->out_npend = 0;

	ctx->status = 0;
	ctx->saved_errors = 0;
	ctx->urb_submission_error = false;

	memcpy(&ctx->config, &default_config, sizeof(ctx->config));

	ctx->read_urb = NULL;
	ctx->write_urb = NULL;

	rt_usb_init_urb_section(&ctx->read_section);

	urb = nrt_usb_create_callback_urb(rt_cdc_read_urb_completed);
	if (!urb)
		return -ENOMEM;

	endpoint = __ffs(ctx->usb_dev->bulk_mask[0]) | 0x80;

	urb->max_buffer_len = RT_CDC_READ_URB_BUFFER_SIZE;
	urb->transfer_buffer_length = RT_CDC_READ_URB_BUFFER_SIZE;
	urb->transfer_flags = URB_RINGBUF | URB_ALLOC_TRANSFER_BUF;
	urb->p_hcd = ctx->usb_dev->p_hcd;
	urb->p_usbdev = ctx->usb_dev;
	urb->pipe = usb_rcvbulkpipe(ctx->usb_dev, endpoint);
	urb->max_packet_size = usb_maxpacket(ctx->usb_dev, urb->pipe);
	urb->p_context = ctx;

	err = nrt_usb_register_urb(urb);
	if (err) {
		nrt_usb_destroy_callback_urb(urb);
		return err;
	}

	ctx->read_urb = urb;

	err = rt_usb_submit_urb(urb);
	if (err)
		goto error;

	urb = nrt_usb_create_callback_urb(rt_cdc_write_urb_completed);
	if (!urb) {
		err = -ENOMEM;
		goto error;
	}

	endpoint = __ffs(ctx->usb_dev->bulk_mask[1]);

	urb->max_buffer_len = RT_CDC_WRITE_URB_BUFFER_SIZE;
	urb->transfer_flags =  URB_ALLOC_TRANSFER_BUF;
	urb->p_hcd = ctx->usb_dev->p_hcd;
	urb->p_usbdev = ctx->usb_dev;
	urb->pipe = usb_sndbulkpipe(ctx->usb_dev, endpoint);
	urb->max_packet_size = usb_maxpacket(ctx->usb_dev, urb->pipe);
	urb->p_context = ctx;

	err = nrt_usb_register_urb(urb);
	if (err) {
		nrt_usb_destroy_callback_urb(urb);
		goto error;
	}

	ctx->write_urb = urb;

	return 0;

error:
	rt_cdc_close(context, user_info);
	return err;
}

static void rt_cdc_destroy_urb(struct rt_urb *urb)
{
	if (!urb)
		return;

	nrt_usb_unregister_urb(urb);

	nrt_usb_destroy_callback_urb(urb);
}

static int rt_cdc_close(struct rtdm_dev_context *context,
			rtdm_user_info_t *user_info)
{
	struct rt_cdc_context *ctx;
	rtdm_lockctx_t lock_ctx;
	struct rt_urb *urb;

	ctx = (struct rt_cdc_context *)context->dev_private;

	rtdm_event_destroy(&ctx->in_event);
	rtdm_event_destroy(&ctx->out_event);
	rtdm_mutex_destroy(&ctx->out_lock);

	rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

	urb = ctx->read_urb;
	ctx->read_urb = NULL;
	ctx->in_npend = 0;

	rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

	rt_cdc_destroy_urb(urb);
	rt_cdc_destroy_urb(ctx->write_urb);

	return 0;
}

static ssize_t rt_cdc_read(struct rtdm_dev_context *context,
			   rtdm_user_info_t *user_info, void *buf,
			   size_t nbyte)
{
	struct rt_cdc_context *ctx;
	rtdm_toseq_t timeout_seq;
	rtdm_lockctx_t lock_ctx;
	u8 *out_pos = (u8 *)buf;
	bool nonblocking;
	size_t read = 0;
	ssize_t ret = -EAGAIN;  /* for non-blocking read */
	size_t block_len;
	u8 *in_pos;

	ctx = (struct rt_cdc_context *)context->dev_private;

	if (nbyte == 0)
		return 0;

	if (user_info && !rtdm_rw_user_ok(user_info, buf, nbyte))
		return -EFAULT;

	rtdm_toseq_init(&timeout_seq, ctx->config.rx_timeout);

	/* non-blocking is handled separately here */
	nonblocking = (ctx->config.rx_timeout < 0);

	/* only one reader allowed, stop any further attempts here */
	if (test_and_set_bit(0, &ctx->in_lock))
		return -EBUSY;

	rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

	while (1) {
		if (ctx->status) {
			if (testbits(ctx->status, RTSER_LSR_BREAK_IND))
				ret = -EPIPE;
			else
				ret = -EIO;
			ctx->saved_errors =
				ctx->status & RTSER_LSR_FRAMING_ERR;
			ctx->status = ctx->urb_submission_error ?
				RTSER_LSR_FRAMING_ERR : 0;
			break;
		}

		if (ctx->in_npend > 0) {
			if (ctx->read_section.length == 0)
				rt_usb_next_urb_section(ctx->read_urb,
							&ctx->read_section);

			in_pos = ctx->read_urb->p_transfer_buffer +
				ctx->read_section.start;
			block_len = (ctx->read_section.length <= nbyte) ?
				ctx->read_section.length : nbyte;

			rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

			if (user_info) {
				if (rtdm_copy_to_user(user_info, out_pos,
						      in_pos,
						      block_len) != 0) {
					ret = -EFAULT;
					goto break_unlocked;
				}
			} else
				memcpy(out_pos, in_pos, block_len);

			read += block_len;
			out_pos += block_len;
			nbyte -= block_len;

			rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

			ctx->in_npend -= block_len;
//			if ((ctx->in_npend -= block) == 0)
//				ctx->ioc_events &= ~RTSER_EVENT_RXPEND;

			ctx->read_section.start += block_len;
			ctx->read_section.length -= block_len;
			if (ctx->read_section.length == 0 && ctx->read_urb)
				 rt_usb_next_urb_section(ctx->read_urb,
							 &ctx->read_section);

			if (nbyte == 0)
				break; /* All requested bytes read. */

			continue;
		}

		if (nonblocking)
			/* ret was set to EAGAIN in case of a real
			   non-blocking call or contains the error
			   returned by rtdm_event_timedwait */
			break;

		ctx->in_nwait = nbyte;

		rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

		ret = rtdm_event_timedwait(&ctx->in_event,
					   ctx->config.rx_timeout,
					   &timeout_seq);
		if (ret < 0) {
			if (ret == -EIDRM) {
				/* Device has been closed -
				   return immediately. */
				return -EBADF;
			}

			rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

			nonblocking = true;
			if (ctx->in_npend > 0) {
				/* Final turn: collect pending bytes
				   before exit. */
				continue;
			}

			ctx->in_nwait = 0;
			break;
		}

		rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);
	}

	rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

break_unlocked:
	/* Release the simple reader lock, */
	clear_bit(0, &ctx->in_lock);

	if ((read > 0) && ((ret == 0) || (ret == -EAGAIN) ||
			   (ret == -ETIMEDOUT) || (ret == -EINTR)))
		ret = read;

	return ret;
}

static ssize_t rt_cdc_write(struct rtdm_dev_context *context,
			    rtdm_user_info_t *user_info, const void *buf,
			    size_t nbyte)
{
	struct rt_cdc_context *ctx;
	rtdm_toseq_t timeout_seq;
	rtdm_lockctx_t lock_ctx;
	void *transfer_buf;
	size_t written = 0;
	size_t block_len;
	int ret;

	if (nbyte == 0)
		return 0;

	if (user_info && !rtdm_read_user_ok(user_info, buf, nbyte))
		return -EFAULT;

	ctx = (struct rt_cdc_context *)context->dev_private;

	rtdm_toseq_init(&timeout_seq, ctx->config.rx_timeout);

	/* Make write operation atomic. */
	ret = rtdm_mutex_timedlock(&ctx->out_lock, ctx->config.rx_timeout,
				   &timeout_seq);
	if (ret)
		return ret;

	while (nbyte > 0) {
		rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

		if (ctx->out_busy) {
			ctx->out_npend = nbyte;
			rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

			ret = rtdm_event_timedwait(&ctx->out_event,
						   ctx->config.tx_timeout,
						   &timeout_seq);
			if (ret < 0) {
				if (ret == -EIDRM) {
					/* Device has been closed -
					   return immediately. */
					return -EBADF;
				}
				if (ret == -EWOULDBLOCK) {
					/* Fix error code for non-blocking
					   mode. */
					ret = -EAGAIN;
				}
				break;
			}
		} else {
			rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

			block_len = nbyte;
			if (block_len > RT_CDC_WRITE_URB_BUFFER_SIZE)
				block_len = RT_CDC_WRITE_URB_BUFFER_SIZE;

			transfer_buf = ctx->write_urb->p_transfer_buffer;
			if (user_info) {
				if (rtdm_copy_from_user(user_info,
							transfer_buf, buf,
							block_len) != 0) {
					ret = -EFAULT;
					break;
				}
			} else
				memcpy(transfer_buf, buf, block_len);

			ctx->write_urb->transfer_buffer_length = block_len;

			ret = rt_usb_submit_urb(ctx->write_urb);
			if (ret < 0) {
				ret = -EIO;
				break;
			}

			written += block_len;
			nbyte -= block_len;
			buf += block_len;
		}
	}

	rtdm_mutex_unlock(&ctx->out_lock);

	if (written > 0 &&
	    (ret == 0 || ret == -EAGAIN || ret == -ETIMEDOUT || ret == -EINTR))
		ret = written;

	return ret;
}

static void rt_cdc_purge_rx(struct rt_cdc_context *ctx)
{
	rtdm_lockctx_t lock_ctx;

	rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

	ctx->in_npend = 0;

	if (ctx->read_urb)
		while (rt_usb_next_urb_section(ctx->read_urb,
					       &ctx->read_section) == 0)
			/* loop */

	rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);
}

static int rt_cdc_ioctl(struct rtdm_dev_context *context,
			rtdm_user_info_t *user_info, unsigned int request,
			void __user *arg)
{
	struct rt_cdc_context *ctx;
	rtdm_lockctx_t lock_ctx;
	int err;

	ctx = (struct rt_cdc_context *)context->dev_private;

	switch (request) {
	case RTIOC_PURGE:
		if ((long)arg & RTDM_PURGE_RX_BUFFER)
			rt_cdc_purge_rx(ctx);
		return 0;

	case RTSER_RTIOC_GET_CONFIG:
		if (user_info)
			err =
			    rtdm_safe_copy_to_user(user_info, arg,
						   &ctx->config,
						   sizeof(struct
							  rtser_config));
		else
			memcpy(arg, &ctx->config,
			       sizeof(struct rtser_config));
		return 0;

	case RTSER_RTIOC_SET_CONFIG: {
		struct rtser_config *config;
		struct rtser_config config_buf;

		config = (struct rtser_config *)arg;

		if (user_info) {
			err =
			    rtdm_safe_copy_from_user(user_info, &config_buf,
						     arg,
						     sizeof(struct
							    rtser_config));
			if (err)
				return err;

			config = &config_buf;
		}

		rtdm_lock_get_irqsave(&ctx->lock, lock_ctx);

		if (testbits(config->config_mask, RTSER_SET_TIMEOUT_RX))
			ctx->config.rx_timeout = config->rx_timeout;

		if (testbits(config->config_mask, RTSER_SET_TIMEOUT_TX))
			ctx->config.tx_timeout = config->tx_timeout;

		/* TODO: add configuration of further features */

		rtdm_lock_put_irqrestore(&ctx->lock, lock_ctx);

		return 0;
	}
	default:
		return -ENOTTY;
	}
}

static const struct rtdm_device __initdata device_tmpl = {
	.struct_version = RTDM_DEVICE_STRUCT_VER,

	.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE,
	.context_size = sizeof(struct rt_cdc_context),
	.device_name = "",

	.open_nrt = rt_cdc_open,

	.ops = {
		.close_nrt = rt_cdc_close,
		.read_rt   = rt_cdc_read,
		.write_rt  = rt_cdc_write,
		.ioctl_rt  = rt_cdc_ioctl,
	},

	.device_class = RTDM_CLASS_SERIAL,
	.device_sub_class = RTDM_SUBCLASS_GENERIC,
	.profile_version = 1,
	.driver_name = "rt_cdc_acm",
	.driver_version = RTDM_DRIVER_VER(0, 5, 0),
	.peripheral_name = "USB CDC ACM",
	.provider_name = "USB4RT",
};

void rt_cdc_exit(void);

int __init rt_cdc_init(void)
{
	struct usb_device *usb_dev = NULL;
	struct rtdm_device *dev;
	int dev_no;
	int err;
	int i;

	for (dev_no = start_index, i = 0; i < MAX_DEVICES; i++) {
		if (!vendor[i] || !product[i])
			continue;

		usb_dev = rt_usb_get_device(vendor[i], product[i], usb_dev);
		if (!usb_dev)
			continue;

		printk("rt_cdc_acm[%d]: device found, vendor=0x%04x, "
		       "product=0x%04x\n", dev_no, vendor[i], product[i]);

		dev = kmalloc(sizeof(struct rtdm_device), GFP_KERNEL);
		err = -ENOMEM;
		if (!dev)
			goto put_dev_out;

		memcpy(dev, &device_tmpl, sizeof(struct rtdm_device));
		snprintf(dev->device_name, RTDM_MAX_DEVNAME_LEN, "rtser%d",
			 dev_no);
		dev->device_id = dev_no++;

		dev->proc_name = dev->device_name;

		dev->device_data = usb_dev;

		err = rtdm_dev_register(dev);
		if (err)
			goto kfree_out;

		device[i] = dev;
	}

	return 0;

kfree_out:
	kfree(dev);

put_dev_out:
	rt_usb_put_device(usb_dev);

	rt_cdc_exit();

	return err;
}

void rt_cdc_exit(void)
{
	struct usb_device *usb_dev;
	int i;

	for (i = 0; i < MAX_DEVICES; i++) {
		if (device[i]) {
			rtdm_dev_unregister(device[i], 1000);
			usb_dev = device[i]->device_data;
			rt_usb_put_device(usb_dev);
			kfree(device[i]);
		}
	}
}

module_init(rt_cdc_init);
module_exit(rt_cdc_exit);
